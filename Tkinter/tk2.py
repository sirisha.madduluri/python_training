#from tkinter import *
import tkinter as tk
from tkinter.ttk import *

main_window = tk.Tk()
main_window.title("Details")
main_window.geometry("500x600")
id_verify = tk.StringVar()
name_verify = tk.StringVar()
marks_verify = tk.StringVar()

label_window = tk.Label(main_window, text = "ID:")
label_window.pack()
entry_id = tk.Entry(main_window, textvariable=id_verify, state='disabled')
entry_id.pack()

label_window = tk.Label(main_window, text = "Name:")
label_window.pack()
entry_name = tk.Entry(main_window, textvariable=name_verify)
entry_name.pack()

label_window = tk.Label(main_window, text = "Marks:")
label_window.pack()
entry_marks = tk.Entry(main_window, textvariable=marks_verify)
entry_marks.pack()

def store_details():
    fl = open("student.csv", mode="a")
    fl.write(entry_id.get()+ "," + entry_name.get() + "," + entry_marks.get() + "\n")
    entry_id.delete(0,"end")
    entry_name.delete(0,"end")
    entry_marks.delete(0,"end")

tk.Button(main_window, text = "Submit", bg="Orange", fg="Red", command = store_details).pack()
tk.mainloop()
