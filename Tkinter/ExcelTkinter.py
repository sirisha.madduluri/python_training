from tkinter import *
import csv

master = Tk()
master.geometry("200x200")
emp_list = []

def add_details():
    emp_dict = {}
    emp_dict['name'] = e1.get()
    emp_dict['age'] = e2.get()
    emp_dict['sal'] = e3.get()
    emp_list.append(emp_dict)
    e1.delete(0, END)
    e2.delete(0, END)
    e3.delete(0, END)

def sore_details():
    print(emp_list)
    with open('employee_details.csv', mode='w') as emp_csv:
        fieldname = ['name', 'age', 'sal']
        data = csv.DictWriter(emp_csv, fieldnames=fieldname)
        data.writeheader()
        for i in emp_list:
            data.writerow(i)

master.destroy()

Label(master, text="Name").grid(row=0)
Label(master, text="age").grid(row=1)
Label(master, text="sal").grid(row=2)
Button(master, text='Add', command=add_details).grid(row=3)
Button(master, text='Store', command=sore_details).grid(row=4)

e1 = Entry(master)
e2 = Entry(master)
e3 = Entry(master)

e1.grid(row=0, column=1)
e2.grid(row=1, column=1)
e3.grid(row=2, column=1)

master.mainloop()