from tkinter import *
from PIL import ImageTk,Image
from tkinter.ttk import *
from tkinter import scrolledtext

#Declaring the variable and assigning the values.
width = 30
font_size = 16
Font_Style = "Arial Bold"

main_window = Tk()
main_window.title("Rufus")
main_window.geometry('450x669')

#creating
lbl_Window = Label(main_window, text='Device Properties-----------------------------', width=width, font=(Font_Style ,font_size))
lbl_Window.grid(column=0, row=0)
lbl_Window.pack()

lbl_Window2 = Label(main_window, text='Device', width=width, font=(Font_Style ,font_size))
lbl_Window2.pack()

chk_state = IntVar()
chk_state.set(1)
Button_Window = Checkbutton(main_window, text="Choose", state='Disabled', var=chk_state)
Button_Window.pack()


Clicked1 = StringVar()

drop1 = OptionMenu(main_window, Clicked1, "None")
drop1.pack()


#dropdown menu
Options = [
    "Non Bootable",
    "FreeDOS",
    "Disk or ISO image(Please select)",
    "ReactOS",
    "Grub 0.24",
    "Syslinux 4.07"
]

Clicked2 = StringVar()
Clicked2.set(Options[2])

drop = OptionMenu(main_window, Clicked2, *Options)
drop.pack()

lbl_Window3 = Label(main_window, text='Please select one dropdown button', width=width, font=(Font_Style ,font_size))
lbl_Window3.pack()



#Start of radio buttons:-
Chk_button = IntVar()
Chk_button.set(0)
rad1 = Radiobutton(main_window, text="RadioButton 1", value=1)
rad2 = Radiobutton(main_window, text="RadioButton 2", value=2)
rad3 = Radiobutton(main_window, text="RadioButton 3", value=3)
rad4 = Radiobutton(main_window, text="RadioButton 4", value=4)
rad5 = Radiobutton(main_window, text="RadioButton 5", value=5)

def Clicked3():
    print(Chk_button.get())

Button_Window1 = Button(main_window, text="Click here", command=Clicked3)
rad1.grid(column=0, row=0)
rad2.grid(column=1, row=0)
rad3.grid(column=2, row=0)
rad4.grid(column=3, row=0)
rad5.grid(column=4, row=0)

main_window.mainloop()


