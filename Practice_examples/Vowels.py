def main():
    s = input("Enter String:")
    count = 0
    vowels = set("aeiou")
    for letters in s:
        if letters in vowels:
            count += 1
    print("Count of the vowels:")
    print(count)

main()