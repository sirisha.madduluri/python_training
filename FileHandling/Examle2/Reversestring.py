#There are 2 files with string values written line by line.
# First file contains 5 strings and 2nd file contains 10 strings.
# How to find the common words in both and display?

# def main():
#
#     with open("ReverseString.txt","r") as f1, open("ReverseString2.txt","r") as f2:
#         file1 = set(x.strip() for x in f1)
#         file2 = set(y.strip() for y in f2)
#         if file2 in file1:
#             print(file2)
#
# if __name__ == "__main__":
#     main()

f1 = open("ReverseString.txt").readlines()
f2 = open("ReverseString2.txt").readlines()
if len(f1) != 0 | len(f2) != 0:
    uniq1 = set(word for line in f1 for word in line.strip().split( ))
    uniq2 = set(words for lines in f2 for words in lines.strip().split( ))
    for word in uniq1:
        for words in uniq2:
            if word == words:
                print(word)

