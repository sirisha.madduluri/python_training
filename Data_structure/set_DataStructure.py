#set is mutable
st = {1,2,3,4}
for x in st:
    print(x)

st = {"apple", "banana", "cherry"}
st.add("berry")
print(st)
st.update(["orange"])
print(st)
st.remove("orange")
print(st)
st.discard("banana")
print(st)
st.pop()
print(st)

st = {"apple", "banana", "cherry",2, 3}
st1 = {1,2,3}
#update and union both will exclude duplicate items
st2 = st.union(st1)
print(st2)
#st1.update(st)
#print(st1)
#st2 = st.intersection(st1)
#print("intersection", st2)

#st2 = st.intersection_update(st1)
#print("Intersection update", st2)

#st2 = st.difference(st1)
#print("differnce", st2)

a = {0, 2, 4, 6, 8}
b = {1, 2, 3, 4, 5}

result = a.intersection_update(b)
print("intersection_update : a", result)

result = a.intersection(b)
print("intersection : ", result)


a = {0, 2, 4, 6, 8}
b = {1, 2, 3, 4, 5}

print("indexing", a[0:4])

a.union(b)
print("union", a)

a = {0, 2, 4, 6, 8}
b = {1, 2, 3, 4, 5}

result = a.difference(b)
print("difference", result)

result = a.difference_update(b)
print("difference update", result)

a = {0, 2, 4, 6, 8}
b = {1, 2, 3, 4, 5}

result = a.symmetric_difference(b)
print("symmetric_difference", result)

result = a.symmetric_difference_update(b)
print("symmetric difference update ", result)

A = {10, 20, 30, 40, 80}
B = {100, 30, 80, 40, 60}

# Modifies A and returns None
A.difference_update(B)

# Prints the modified set
print(A)







