#tuple is faster compared to list because it uses less memory and it is immutable
#tuple are generally used for smaller group of similar items
#tuple - heterogeneous sequence
# sequence unpacking

#the value stored in the tuple can be any type and they are indexed by integers.

tple = (1,2,3,4)
print(tple)
print("type of the given input", type(tple))

tple = 1,2,3,4
print(tple)
print(type(tple))

tple = (1,)
print(tple)
print(type(tple))

tple = (1, 0)
tple += (1,)
print(tple)

tple = (1,2,3,4)
#they are two methods avaiable
#1. index - which gives the position of the value
#2. count - count the number of occurences of the value
print(tple.count(4))
print(tple.index(4))
print(len(tple))
print(tple[:2])

tple = ()
print("A blank tuple: ",tple)

tple = (33, 3.3, 3+4j)
print("Mixed number", tple)

tple = (33, "33" , [3,3])
print("Mixed Datatype", tple)

tple = (('x','y','z'), ('X','Y','Z'))
print("tuple of tuple", tple)

tple = ({3,5,6})
print("dictionaries in tuple", type(tple))

tple = tuple({3,5,6})
print("dictionaries in tuple", type(tple))

tple = ([1,2,3,4])
print("list in tuple", type(tple))

tple = tuple([1,2,3,4])
print("typecasting to tuple", type(tple))

#tple = (1,2,3,4)
#tple.append(5)
#print("Tuple is immutable",tple)
#tple.pop()
#print("output when pop", tple)

tple1 = (1,2,3,4)
tple2 = ('a','b','c','d')
a = tple1 + tple2
print("add two tuples: ", a)
print(len(a))

#tple = (1,2,3,4,5,6)
#tple[0] = 2
#print(tple)

tple = (1,2,3)
#this is called sequence unpacking
x,y,z = tple
print(x,y,z)

a = 'abc'
b = 'def ghi'
a,b = b,a
print(a,b)
print("split", a.split())


def func(*arg):
    print("actual input", arg)
    for x in arg:
        print("expected result", x)

func(1,2,3,4)

init_tuple_a = 1, 2
init_tuple_b = (3, 4)

print("adding two tuple", init_tuple_a + init_tuple_b)

[print(sum(x)) for x in [init_tuple_a + init_tuple_b]]

init_tuple = [(0, 1), (1, 2), (2, 3)]
result = sum(n for _, n in init_tuple)
print("result", result)

