#dict in python is accessed by keys not by their position.
#dict = {}
#print(dict)
#print(len(dict))
#print(type(dict))

dict = {1: 'apple', 2: 'ball', 'cat':[1,2]}
#for x in dict:
    #print("for loop", x)
    #print("second for loop", dict[x])
    #print("printing values",dict.values())
    #print("Printing items",dict.items())
if 1 in dict:
    print("it exists in dict")
else:
    print("no")
dict["colour"] = ["red","black","blue"]
print("length", dict)

#print(dict.index[0])
print(dict.keys())
print(dict.values())
print(dict[1])
#return a list of items
print("print items",dict.items())
print(dict.clear())
print("clearing the dict", dict)
dict = {1: 'apple', 2: 'ball', 'cat':[1,2]}
print("get the dict: ",dict.get(2))
print(dict)
#pop - delete remove the corresponding item from dictionary
print("pop the dict: ",dict.pop(2))
print(dict)
#popitem - removes and return a pair , not sure which one will be removed
print("popitems from the dict", dict.popitem())
print(dict)

dict1 = {1: 'apple', 2: 'ball', 'cat': [1,2]}
dict2 = {'a': 10 , 'b': 11, 'c': 13}
dict2 = dict1.copy()
print("copying", dict2)
print("copying2", dict1)


dict = {1: 'apple', 2: 'ball', 'cat': [1,2]}
#create a new item
print(dict.setdefault('third',''))
print("set default", dict)
print(dict.fromkeys('cat'))
print(dict)

#more than one entity of the keyword cannot be used.
#The values in the dictionary can be of any type while the keys must be immutable like numbers, tuples or strings.

dict1 = {1: ('apple', 'a', 'c') , 2: 'ball', 'cat': [1,2]}

letters = {'a': 'apples', 'b': 'banana', 'c': 'coconut', 'd': 'durian'}
for x in letters.items():
    print("items",x)
for x in letters.keys():
    print("keys", x)
for x in letters.values():
    print("values", x)

dict = {0: 10, 1: 20}
dict[2] = 30

print(dict)
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50,6:60}
dic4 = {}

for d in (dic1, dic2, dic3): dic4.update(d)
print(dic4)

a = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
def kpres(x):
    if x in a:
        print("key is present in dictionary")
    else:
        print("key is not present in dictionary")

kpres(5)
kpres(9)

def merge(a, b):
    return(b.update(a))

a = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
b = {'a': [1, 2], 'b': (3, 4), 'c': {4, 5}}

print(merge(dict1,dict2))
print(dic2)






