#list - homogeneous
from random import choice
lst = ['a','b','c','d']
lst[0] = 'q'
print(lst)

#slicing
lst = [1,2,3,4,5,6,7,8]
print("sliced",lst[::2])
#indexed
print(lst[1])

f = [1, 100, 30, 900, 80, 50]
f.sort()
print("f = ", f)
g = [1, 2, 3, 4, 5]
print("sum", sum(g))
a = f.index(50)
print("a = ", a)
b = f.count(0)
print("b = ", b)
c = f.pop()
print("c = ", c)
f.insert(0, 25)
print("d = ",f)
f.extend(g)
print("e = ", f)

#f = [1, 2, 3, 1, 2, 1, 2, 3, 2, 1]
#print(f.insert(2,2))

data = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
def fun(m):
    v = m[0][0]
    print("print v: ", v) # v= 1
    for column in m:
        print("column : ", column) #column = 3, 4
        print("m : ", m) #m = [1, 2], [3, 4]
        for element in column:
            print("elements : ", element) #3 and 4
            if v < element: v = element # if 1 < 3 : 1

    return v
print(fun(data[0]))

arr = [[1, 2, 3, 4],
       [4, 5, 6, 7],
       [8, 9, 10, 11],
       [12, 13, 14, 15]]
print("array", arr[0])
for i in range(0, 4):
    print(arr[i].pop())

def f(i, values = []):
    print("Printing i values", i)
    values.append(i)
    print (values)
    return values
f(1)
f(2)
f(3)

arr = [1, 2, 3, 4, 5, 6]
#for x in range(0, 6):
#    print(x, end=",")
#    print("first x", x) #0,1,2,3,4,5
for i in range(1, 6): #1,2,3,4,5
    arr[i - 1] = arr[i]
    print("array of i : ", i - 1) #0,1,2,3,4
    print("arr[i] : ", i) #1,2,3,4,5
for i in range(0, 6): # 1 in
    #print("arra[i] = " , arr[i] , end = ",")
    print("arr[i] 2nd : ", arr[i] )

a = [3, 4]
b = [9, 4]

[print(sum(x)) for x in [a + b]]
a = [34,67,78,12,45,199]
print(max(a))
print(min(a))

a = ()
if len(a) == 0:
    print("its an empty string")
else:
    print("its not an empty string")















