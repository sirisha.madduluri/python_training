"""Write a Python program to get a single string from two given strings, separated by a space and swap the first two characters of each string. Go to the editor
Sample String : 'abc', 'xyz'
Expected Result : 'xyc abz'"""

"""str1 = "abc"
str2 = "xyz"

str3 = str1 + str2
print(str3)

str3 = str1
str1 = str2
str2 = str3

print(str1)
print(str2)

str3 = str1 + str2
print(str3)"""

def func(st1, st2):
    if len(st1) and len(st2) > 2:
        a = st2[:2] + st1[2:]
        b = st1[:2] + st2[2:]
        print("First string swapped is : ", a)
        print("Second string swapped is : ", b)
    else:
        return "The input you entered is invalid, Please enter the string which has more than 2 characters"

# " " seperated by space.
    return a + " " + b

print(func(input("Enter first string: ?\n"), input("Enter second string: ?\n")))

#done
