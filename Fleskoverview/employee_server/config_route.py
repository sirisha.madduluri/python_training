from flask import Flask, request, g, render_template, redirect, flash, send_file,url_for, send_from_directory
import employee_server.db.db_config as esd
from employee_server.api import *
import os
from werkzeug.utils import secure_filename
import numpy as np
from employee_server.utils import uitils as emu
import json

app = Flask(__name__)

UPLOAD_FOLDER = os.getcwd()+ '/' + 'static'
app.secret_key = 'secret key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_COUNT_LENGTH'] = 16 * 1024 * 1024
ALLOWED_EXTENSIONS  = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
base_url = 'http://localhost:8080'

@app.before_request
def before_request():
    print('db')
    g.db = esd.get_db()

@app.route('/api/employees')
def get_employee_list():

    return fetch_emp_list()


# post request to store employee details
@app.route('/api/storeemployee', methods=['POST'])
def store_employee():

    return store_employee()

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/fileuploadpage')
def upload_form():
	return render_template('upload.html')

@app.route('/fileupload', methods=['POST'])
def render_home():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            flash('File(s) successfully uploaded')
            return redirect('/fileuploadpage')

@app.route('/userdetails/static')
def download_file():
    """
    download image path
    :return: this returns all the images stored in path
    """
    #For windows you need to use drive name [ex: F:/Example.pdf]
    path = os.getcwd() + '/static/'
    return send_file(path, attachment_filename='images.jpg')

@app.route('/api/image/profile/<filename>')
def show_index(filename):
    filename = 'http://127.0.0.1:8080/uploads/' + filename
    #full_filename = os.getcwd() + '/static/'+ filename
    return render_template("img_show.html", filename = filename)

@app.route('/uploads/<filename>')
def send_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)

