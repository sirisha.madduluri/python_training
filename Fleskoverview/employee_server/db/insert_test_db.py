import sqlite3 as sq
import employee_server.db.db_config as esd

conn = sq.connect(esd.DATABASE_PATH)
print('Opend database successfully')

id = input('Enter user id: \n')
name = input('Enter user name: \n')
dob = input('Enter user dob: \n')
address = input('Enter user address: \n')
salary = input('Enter user salary: \n')

query = ('INSERT INTO Employee (ID, NAME, DOB, ADDRESS, SALARY) '
         ' VALUES (:ID, :NAME, :DOB, :ADDRESS, :SALARY);')

param = {
    'ID': id,
    'NAME': name,
    'DOB': dob,
    'ADDRESS': address,
    'SALARY': salary
}


conn.execute(query, param)

conn.commit()
conn.close()

