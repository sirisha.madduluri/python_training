import sqlite3 as sq

import employee_server.db.db_config
import employee_server.db.db_config as esd

conn = sq.connect(esd.DATABASE_PATH)
cur = conn.cursor()

"""##########"""
conn.execute('CREATE TABLE ' + employee_server.db.db_config.TABLE_NAME + ' (' +
             'ID INT PRIMARY KEY NOT NULL, '
             'NAME TEXT NOT NULL, '
             'DOB TEXT NOT NULL, '
             'ADDRESS CHAR(50),'
             'SALARY REAL' + ' );')


conn.commit()
conn.close()