'''Composition of Decorators
'''
"""
<p>lorem ipsum, {0} dolor sit amet</p>
"""
def get_text(name):
    return "lorem ipsum, {0} dolor sit amet".format(name)

def p_decorate(greet_someone):
    def func_wrapper(name):
        data = greet_someone(name)
        res = "<p>{0}</p>".format(data)
        return res

    return func_wrapper

def main():
    my_get_text = p_decorate(get_text)
    print(my_get_text("John"))

main()

