'''Define functions inside other functions
'''
def greet(name):
    def get_message():
        return "Hello " + name

    result = get_message
    return result

"""....................................."""
get = greet("John")
print(get())





