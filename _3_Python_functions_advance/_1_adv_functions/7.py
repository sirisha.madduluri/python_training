'''Python makes creating and using decorators a
bit cleaner and nicer for the programmer through some syntatic sugar
The name of the decorator should be perpended with an @ symbol.
'''
def p_decorate(greet_someone):
   def func_wrapper(name):
       res = "<p>{0}</p>".format(greet_someone(name))
       return res

   return func_wrapper

@p_decorate
def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)
"""..........................................."""
print(get_text("John"))