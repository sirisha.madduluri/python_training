"""

"""
f = lambda x: 2 * x
print(f(2))

"""

"""
f = lambda x, y: x + y
print
f(2, 3)

"""

"""
f = lambda x, y=3: x + y
print(f(2))
print(f(4, 1))

"""
"""
import operator
f = lambda *x: reduce(operator.add, x)
print(f)
print(f(1))
print(f(1, 2))
print(f(1, 2, 3))
