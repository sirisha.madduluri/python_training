


def lname_deco(get_emp_li):
    def function_wrapper():
        f_name_li = get_emp_li()
        l_name_li = ['mev', 'hanry', 'dec']
        res_li = []
        row = 0
        for i in f_name_li:
            res_li.append(i + " " + l_name_li[row])
            row += 1

        return res_li

    return function_wrapper

@lname_deco
def get_emp_list():
    # first name and last name as colm
    return ['jhon', 'mark', 'sofiya']

def main():
    print(get_emp_list())



main()