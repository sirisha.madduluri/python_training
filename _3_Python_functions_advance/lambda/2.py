# Program to filter out only the even items from a list

my_list = [1, 5, 4, 6, 8, 11, 3, 12]

ty = filter(lambda x: (x%2 == 0) , my_list)
print(type(ty))
print(ty)

new_list = list(filter(lambda x: (x%2 == 1) , my_list))
print(new_list)

# list(filter(lambda x: (x % 2 == 0), my_list))

# Output: [4, 6, 8, 12]


