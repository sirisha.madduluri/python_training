# from functools import reduce
#
# li = [5, 8]
#
# sum = reduce((lambda x, y: x + y), li)
# print(sum)
#
# """reduce using lambda"""

"""generator in python"""

def incrmnt(value):
    return value + 1

x = incrmnt(10)
print(x)

x = incrmnt
print(x)

def inc_gen(value):
    yield value + 1
    yield value + 2
    yield value + 3

x = inc_gen(10)
print(x.__next__())
print(x.__next__())
print(x.__next__())

for i in inc_gen(10):
    print(i)

x = []
x = inc_gen(10)
print(x)

for i in x:
    print(str(i) + "........")