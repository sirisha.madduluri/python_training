# Program to show the use of lambda functions
"""
lambda arguments: expression
"""
add = lambda x,y: x + y


# Output: 10
print(add(5, 10))

def add(x, y):
   return x + y

print(add(5, 10))

""" 
a = lambda x, y: x * y

is same as

def double(x, y):
   return x * y
"""
def some(*x):
    print(x)

print(some(1, 2,3,4))