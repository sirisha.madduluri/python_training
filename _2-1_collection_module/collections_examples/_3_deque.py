import collections


dq = collections.deque([1, 2, 3, 4, 5])
print(dq)

dq.appendleft(6)
print(dq)

dq.pop()
print(dq)

dq.popleft()
print(dq)

dq.pop()
print(dq)

a = [1, 2, 3, 4, 5, 6]


