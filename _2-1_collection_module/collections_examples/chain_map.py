import collections

d1 = {}
d1['a'] = 1
d1['b'] = 2
d1['c'] = 3

d2 = {}
d2['d'] = 1
d2['e'] = 2
d2['c'] = 3

d3 = {}
d3['d'] = 1
d3['h'] = 2
d3['i'] = 3

cmap = collections.ChainMap(d1, d2, d3)
print(list(cmap.maps))
print(list(reversed(cmap.maps)))
print(list(d1.keys()))
print(list(d1.values()))
print(list(cmap.keys()))
print(list(cmap.values()))

#
# """
# example second
# """
# # Please select Python 3 for running this code in IDE
# # Python code to demonstrate ChainMap and
# # reversed() and new_child()
#
# # importing collections for ChainMap operations
# import collections
#
# # initializing dictionaries
# dic1 = { 'a' : 1, 'b' : 2 }
# dic2 = { 'b' : 3, 'c' : 4 }
# dic3 = { 'f' : 5 }
#
# # initializing ChainMap
# chain = collections.ChainMap(dic1, dic2)
#
# # printing chainMap using map
# print ("All the ChainMap contents are : ")
# print (chain.maps)
#
# # using new_child() to add new dictionary
# chain1 = chain.new_child(dic3)
#
# # printing chainMap using map
# print ("Displaying new ChainMap : ")
# print (chain1.maps)
#
# # displaying value associated with b before reversing
# print ("Value associated with b before reversing is : ",end="")
# print (chain1['b'])
#
# # reversing the ChainMap
# chain1.maps = reversed(chain1.maps)
#
# # displaying value associated with b after reversing
# print ("Value associated with b after reversing is : ",end="")
# print (chain1['b'])
