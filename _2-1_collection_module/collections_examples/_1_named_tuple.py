"""
The collections module provides alternatives to
built-in container data types such as list, tuple and dict.
"""
"""
Example one
"""
import collections
person = collections.namedtuple('person', ['name', 'subject', 'marks'])
s1 = person('Narendra', 'subject', '28')
print(s1)

"""Example two"""
comp_st = collections.namedtuple('st1',['employee', 'department'])
emp_st = collections.namedtuple('st2',['name', 'age', 'sal', 'address'])
dept_st = collections.namedtuple('st3',['names'])

employee = emp_st('Narendra', '28', '20lac', 'Bangalore')
department = ['Dev', 'test', 'salse', 'HR']
departmane = dept_st(department)
companny = comp_st(employee, departmane)
print(companny.employee.name)
print(companny.employee.address)
print(companny.employee.age)
print(companny.employee.address)
print(companny.department.names)



