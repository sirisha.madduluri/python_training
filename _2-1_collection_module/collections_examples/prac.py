import collections as cs
# named tuple
# student = cs.namedtuple("stu", ['name', 'year', 'address', 'parent'])
# parent = cs.namedtuple('prt', ['father', 'mother'])
# pt1 = parent('mark', 'july')
# st1 = student('john', '1st year', 'ny', pt1)
# print(st1.name)
# print(st1.year)
# print(st1.address)
# print(st1.parent.father)
# print(st1.parent.mother)

# student = cs.namedtuple("stu", ['name', 'year'])
# st1 = student('john', '1')
# print(id(st1))
# st1 = student('john', '2')
# print(st1)