import collections as cs

stu = cs.namedtuple('stu', ['name', 'marks', 'prt'])
prt = cs.namedtuple('prt', ['mname', 'fname'])
s2 = prt('jul', 'mark')
s1 = stu('john', 98, 'jh')
print(s1.name)
print(s1.marks)
print(s1.prt)
# print(s1.prt.fname)
# print(s1.prt.mname)

t = ('john', 98, ('jul', 'mark'))
print(t)
print(t[2][1])