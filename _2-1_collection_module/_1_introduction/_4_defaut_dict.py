from collections import defaultdict


dict_marks = defaultdict(set)
dict_marks['a'] = 1
dict_marks['b'] = 2
dict_marks['c'] = 3

print(dict_marks.items())

print(dict_marks)

ab = {}

ab['a'] = 1
ab['b'] = 2
ab['c'] = 3

print(ab['d'])