# # import collections as cs
# # students = cs.namedtuple('stu',['name','subject','marks','mother','father'])
# # mother = collections.namedtuple('mtr', ['name', 'age'])
# # father = collections.namedtuple('ftr', ['name', 'age'])
# #
# # m = mother('July', 26)
# # f = father('Jack', 31)
# # s1 = students('john', 'math', 28, m, f)
# #
# # print(s1.name)
# #print(s1.mother.)
#
# import collections
# student = collections.namedtuple('stu', ['name', 'age', 'ptr'])
# ptr = collections.namedtuple('ptr', ['name', 'age', 'gptr'])
# gptr = collections.namedtuple('gptr', ['name', 'age'])
#
# gp1 = gptr('maevrick', 50)
# p1 = ptr('mark', 31, gp1)
# s1 = student('john', 20, p1 )
# s2 = student('john', 20, p1 )
# print(id(s1))
# print(id(s2))
# print(s1.name)
# print(s1.age)
# print(s1.ptr.gptr.name)
#
#
#

import collections

# dq = collections.deque([1, 2, 3, 4, 5])
# li = [1, 2, 3, 4,5]
# print(li)
# li.remove(4)
# print(li)
# print(dq)
# dq.remove(4)
# print(dq)
