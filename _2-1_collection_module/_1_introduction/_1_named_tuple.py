"""
The collections module provides alternatives to
built-in container data types such as list, tuple and dict.
"""

import collections
students = collections.namedtuple('stu',['name','subject','marks','mother','father'])

# info = students('john', 'math', '29', 'jack', 'july')


#parents = collections.namedtuple('ptr', ['name', 'age', ])
mother = collections.namedtuple('ptr', ['name', 'age', ])

father = collections.namedtuple('ptr', ['name', 'age', ])

#s2 = parents('JAck', 28)
m = mother('July', 26)
f = father('Jack', 31)
s1 = students('john', 'math', 28, m, f)

print(s1.name)
print(s1.marks)
print(s1.subject)
print(s1.mother.name)
print(s1.father.name)
print(s1.father.age)



