import collections
od = {}
od['a'] = 1
od['b'] = 2
od['c'] = 3
print(od)
od.popitem()
print(od)

d1 = collections.OrderedDict()
d1['a'] = 1
d1['b'] = 2
d1['c'] = 3

print(d1)
d1.popitem(False)
d1['d'] = 4
print(d1)
d1.popitem(True)
print(d1)