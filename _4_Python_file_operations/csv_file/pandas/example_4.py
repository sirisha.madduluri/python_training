import pandas
df = pandas.read_csv('hr_data.csv',
            index_col='Employee', 
            parse_dates=['Hired'],
            header=0,
            names=['Employee', 'Hired','Salary', 'Sick Days'])
print(df)

