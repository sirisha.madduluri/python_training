"""
To use a different column as the DataFrame index, add the index_col optional parameter:
"""
import pandas
df = pandas.read_csv('hr_data.csv', index_col='Hire Date')
print(df)