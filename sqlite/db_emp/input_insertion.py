import sqlite3

conn = sqlite3.connect('emp_db.db')
print("Opened database successfully")

id = input('Enter user id: \n')
name = input('Enter user name: \n')
age = input('Enter user age: \n')
address = input('Enter user address: \n')
salary = input('Enter user salary: \n')


query = ('INSERT INTO Employee (ID,NAME,AGE,ADDRESS,SALARY) '
         'VALUES (:ID, :NAME, :AGE, :ADDRESS, :SALARY);')

params = {
        'ID': id,
        'NAME': name,
        'AGE': age,
        'ADDRESS': address,
        'SALARY': salary
    }

conn.execute(query, params)


conn.commit()

print("Records created successfully")
conn.close()

