# Define a function `plus()`
def plus(a, b):
    return a + b

# Create a `Summation` class
class Summation(object):
    def sum(self, a, b):
        self.contents = a + b
        return self.contents

# Instantiate `Summation` class to call `sum()`
sumInstance = Summation()
sumInstance.sum(1,2)