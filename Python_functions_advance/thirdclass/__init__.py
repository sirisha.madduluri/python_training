# An example of a recursive function to
# find the factorial of a number

def calc_factorial(x):
    """This is a recursive function
    to find the factorial of an integer"""

    if x == 1:
        return 1
    else:
        return (x * calc_factorial(x-1))

num = 4
print("The factorial of", num, "is", calc_factorial(num))


#...............................................

# A simple generator function
def example_gen():
    n = 1
    print('first')
    yield n

    n += 1
    print('second')
    yield n

    n += 1
    print('end')
    yield n
for item in example_gen():
    print(item)


class Series(object):
    def __init__(self, low, high):
        self.current = low
        self.high = high

    def __iter__(self):
        return self

    def __next__(self):
        if self.current > self.high:
            raise StopIteration
        else:
            self.current += 1
            return self.current - 1

n_list = Series(1,10)
print(list(n_list))






it = iter(str)

print(it.next())
print(it.next())
print(it.next())

print(list(it))