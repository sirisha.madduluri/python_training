import abc


class parent:
    def geeks(self):
        pass


class child(parent):
    def geeks(self):
        print("child class")


p = parent()
p.geeks()

c = child()
c.geeks()