list1 = ['little','blue','widget', 'cup']
list2 = ['there','is','a','little','blue','cup','on','the','table']

list3 = set(list1)&set(list2) # we don't need to list3 to actually be a list
print(list3)

list4 = sorted(list3, key = lambda k : list1.index(k))
print(list4)