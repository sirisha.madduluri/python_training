'''Assign functions to variables
'''
def greet(name):
    return "hello " + name


""".............................."""

print(greet('john'))
greet_someone = greet
print(greet_someone("John"))



