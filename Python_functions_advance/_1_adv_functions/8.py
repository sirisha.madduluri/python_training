'''Now let's consider we wanted to decorate our get_text function by 2 other
functions to wrap a div and strong tag around the string output.'''
"""With the basic approach, decorating get_text would be along the lines of"""

def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

def strong_decorate(func):
    def func_wrapper(name):
        return "<strong>{0}</strong>".format(func(name))
    return func_wrapper

def div_decorate(func):
    def func_wrapper(name):
        return "<div>{0}</div>".format(func(name))
    return func_wrapper
"""............................................................"""

get_text = div_decorate(p_decorate(strong_decorate(get_text)))
print(get_text('john'))

