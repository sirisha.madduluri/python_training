def greet(name):
    def get_message():
        return 'get_message' + name

    def get_message1():
        return 'get_message1' + name

    def get_message2():
        return 'get_message2 ' + name

    result = None
    if name == 'john':
        result = get_message
    elif name == 'mark':
        result = get_message1
    else:
        result = get_message2

    return result



get = greet('mark')
print(get())