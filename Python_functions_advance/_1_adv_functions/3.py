    '''Functions can be passed as parameters to other functions
'''
def greet(name):
   return "Hello " + name

def call_func(greet_someone):
    # greet_someone is now equal to greet
    other_name = "John"
    return greet_someone(other_name)

"""...................................."""

print(call_func(greet))


