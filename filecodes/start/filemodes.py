# 'r'	Open a file for reading. (default)
# 'w'	Open a file for writing. Creates a new file if it does not exist or truncates the file if it exists.
# 'x'	Open a file for exclusive creation. If the file already exists, the operation fails.
# 'a'	Open for appending at the end of the file without truncating it. Creates a new file if it does not exist.
# 't'	Open in text mode. (default)
# 'b'	Open in binary mode.
# '+'	Open a file for updating (reading and writing)

def checkfile_mode():
    fl_mode = input('enter file mode: ')
    file_name = 'test1.txt'
    file_object = None
    try:
        file_object = open(file_name, fl_mode)
        file_operations(fl_mode, file_object)
    except Exception:
        raise Exception()
    finally:
        file_object.close()


def file_operations(argument, file_object):
    if argument == 'r':
        print('file mode is: {}'.format(argument))
        output = file_object.read(100)
        print(output)
    elif argument == 'w+':
        print('file mode is: {}'.format(argument))
        file_object.write('ABC')
        data = file_object.read()
        print(data)
    elif argument == 'x':
        print('file mode is: {}'.format(argument))
        file_object.write('123')
    elif argument == 'a':
        print('file mode is: {}'.format(argument))
        file_object.write('123')
        
    elif argument == 'rb':
        print('file mode is: {}'.format(argument))
        f = open('1.mp3', 'rb')
        file_content = f.read()
        print(file_content)
        f.close()

checkfile_mode()



"""
1	
r

Opens a file for reading only. The file pointer is placed at the beginning of the file. This is the default mode.

2	
rb

Opens a file for reading only in binary format. The file pointer is placed at the beginning of the file. This is the default mode.

3	
r+

Opens a file for both reading and writing. The file pointer placed at the beginning of the file.

4	
rb+

Opens a file for both reading and writing in binary format. The file pointer placed at the beginning of the file.

5	
w

Opens a file for writing only. Overwrites the file if the file exists. If the file does not exist, creates a new file for writing.

6	
wb

Opens a file for writing only in binary format. Overwrites the file if the file exists. If the file does not exist, creates a new file for writing.

7	
w+

Opens a file for both writing and reading. Overwrites the existing file if the file exists. If the file does not exist, creates a new file for reading and writing.

8	
wb+

Opens a file for both writing and reading in binary format. Overwrites the existing file if the file exists. If the file does not exist, creates a new file for reading and writing.

9	
a

Opens a file for appending. The file pointer is at the end of the file if the file exists. That is, the file is in the append mode. If the file does not exist, it creates a new file for writing.

10	
ab

Opens a file for appending in binary format. The file pointer is at the end of the file if the file exists. That is, the file is in the append mode. If the file does not exist, it creates a new file for writing.

11	
a+

Opens a file for both appending and reading. The file pointer is at the end of the file if the file exists. The file opens in the append mode. If the file does not exist, it creates a new file for reading and writing.

12	
ab+

Opens a file for both appending and reading in binary format. The file pointer is at the end of the file if the file exists. The file opens in the append mode. If the file does not exist, it creates a new file for reading and writing.
"""