def file_write_opt():
    file = open('testfile.txt', 'w')

    file.write('Hello World')
    file.write('This is our new text file')
    file.write(' and this is another line.')
    file.write('Why? Because we can.')

    file.close()


def read_file_op():
    file = open('testfile.txt', 'r')
    print(file.read(5))
    ''' see all methods of file read'''
    file.close()

def for_loop_file():
    f = open('testfile.txt', 'r')
    for line in f:
        print(line)

def with_key():
    with open('testfile.txt', 'r') as f:
        data = f.readlines()
        print(data)

with_key()