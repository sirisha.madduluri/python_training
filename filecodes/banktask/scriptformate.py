import xlrd
import csv
import xlsxwriter
import pathlib
import openpyxl
import xlwt


filename = "2020-01-06-NSE-EQ"
ffilename = filename +'.txt'
wbname = 'Report' +".xlsx"


def initexl():
    """
    The function reads data from csv data but now only one sample is possible to add.
    :return: None
    """
    if not pathlib.Path(wbname).exists():
        wb = xlsxwriter.Workbook(wbname)
        with open(ffilename, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                sheet = wb.add_worksheet(row['<ticker>'])
                sheet.write(0, 0, 'Stock Name')
                sheet.write(1, 0, row['<ticker>'])
                sheet.write(0, 1, '<date>')
                sheet.write(1, 1, '<open>')
                sheet.write(2, 1, '<high>')
                sheet.write(3, 1, '<low>')
                sheet.write(4, 1, '<close>')
                sheet.write(5, 1, '<volume>')
                sheet.write(6, 1, '<o/i>')
                print("+++++++++++++++++++++++++++++++++++++++++++++++++ " +row['<ticker>'] )
            wb.close()
            data_writing()
    else:
        data_writing()


def data_writing():
    """
    This function is writing the data to exl one sample at the time.
    :return: None
    """
    wbk = openpyxl.load_workbook(wbname)
    with open(ffilename, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        index = 0;
        wb = xlrd.open_workbook(wbname)
        for row in csv_reader:
            col = wb.sheet_by_index(index).ncols + 1
            wks = wbk[row['<ticker>']]
            wks.cell(row=1, column=col).value = row['<date>']
            wks.cell(row=2, column=col).value = row['<open>']
            wks.cell(row=3, column=col).value = row['<high>']
            wks.cell(row=4, column=col).value = row['<low>']
            wks.cell(row=5, column=col).value = row['<close>']
            wks.cell(row=6, column=col).value = row['<volume>']
            wks.cell(row=7, column=col).value = row['<o/i>']

            index += 1

    wbk.save(wbname)
    wbk.close


# providing the sanple code for adding all the records on run
def main():
    """
    The business logic
    :return: None
    """
    initexl()


if __name__ == '__main__':
    main()
