import csv
import xlsxwriter
import xlrd
import xlwt
import openpyxl

wbkName = 'Bank.xlsx'

def initexl():
    wb = xlsxwriter.Workbook(wbkName)
    with open('2020-01-01-NSE-EQ.txt', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            sheet = wb.add_worksheet(row['ticker'])
            sheet.write(0, 0, 'Stock Name')
            sheet.write(1, 0, row['ticker'])
            sheet.write(0, 1, 'date')
            sheet.write(1, 1, 'open')
            sheet.write(2, 1, 'high')
            sheet.write(3, 1, 'low')
            sheet.write(4, 1, 'close')
            sheet.write(5, 1, 'volume')
            sheet.write(6, 1, 'o/i')
        wb.close()


def rdelx(index=0):
    wb = xlrd.open_workbook(wbkName)
    col = wb.sheet_by_index(index).ncols + 1
    with open('2020-01-01-NSE-EQ.txt', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        wbk = openpyxl.load_workbook(wbkName)
        for wks in wbk.worksheets:
            wks.cell(row=0, column=col).value = 123
            wks.cell(row=1, column=col).value = 123
            wks.cell(row=2, column=col).value = 123
            wks.cell(row=3, column=col).value = 123
            wks.cell(row=4, column=col).value = 123
            wks.cell(row=5, column=col).value = 123
            wks.cell(row=6, column=col).value = 123

        wbk.save(wbkName)
        wbk.close




def main():
    initexl()
    rdelx()


if __name__ == '__main__':
    main()




