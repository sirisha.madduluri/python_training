"""
writing csv file
"""
import csv

with open('employee_file1.csv', mode='w') as employee_file:
    emp = csv.writer(employee_file, delimiter=',')
    emp.writerow(['name', 'dept', 'month'])
    emp.writerow({'name', 'dept', 'month'})
    emp.writerow({'name', 'dept', 'month'})
    emp.writerow({'name', 'dept', 'month'})
    emp.writerow({'name', 'dept', 'month'})

