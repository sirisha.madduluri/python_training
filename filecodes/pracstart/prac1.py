import csv

with open('employee.csv', mode='a+') as emp_file:
    fields = ['id', 'name', 'address']
    emp_writer = csv.DictWriter(emp_file, fieldnames=fields)
    emp_writer.writeheader()
    """
    data fetching code
    
    """
    id = input('Enter id: ')
    name = input('Enter name: ')
    addres = input('Enter address: ')

    emp_dict = {
        'id':id,
        'name':name,
        'address':addres
    }
    emp_writer.writerow(emp_dict)
